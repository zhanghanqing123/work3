package com.xuecheng.controller;

import com.xuecheng.entity.Student;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "Hello Api 服务测试",description = "对Hello Api 服务提供接口")
public interface IStudentController {


    @ApiOperation("综合参数方式的测试方法")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "学员的编号",required = true,dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "name",value = "学员的名称",required = true,dataType = "String",paramType = "query")
    })
    Student mofidyStudentBynNum(@PathVariable("id") String id, @RequestParam("name") String name, @RequestBody Student student);
}
