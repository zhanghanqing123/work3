package com.xuecheng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
