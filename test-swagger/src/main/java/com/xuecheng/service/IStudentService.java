package com.xuecheng.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.entity.Student;

public interface IStudentService extends IService<Student>{
}
