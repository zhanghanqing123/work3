package com.xuecheng.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.entity.Student;
import com.xuecheng.mapper.StudentMapper;
import com.xuecheng.service.IStudentService;
import org.springframework.stereotype.Service;

@Service
public class IStudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {
}
