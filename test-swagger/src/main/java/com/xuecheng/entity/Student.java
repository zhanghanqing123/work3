package com.xuecheng.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "学员的实体类",description = "对学院属性的封装")
public class Student {
    //学员编号
    @ApiModelProperty("学员的编号")
    private String stuNo;

    //学院名称
    @ApiModelProperty("学员的名称")
    private String name;
    //学员年龄
    @ApiModelProperty("学员的年龄")
    private int age;
    //学员地址
    @ApiModelProperty("学员的地址")
    private String address;
}
