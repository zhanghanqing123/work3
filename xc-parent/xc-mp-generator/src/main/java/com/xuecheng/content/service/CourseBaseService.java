package com.xuecheng.content.service;

import com.xuecheng.content.entity.CourseBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程基本信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2021-11-07
 */
public interface CourseBaseService extends IService<CourseBase> {

}
